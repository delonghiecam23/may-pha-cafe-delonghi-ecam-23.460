Thông tin chi tiết Máy pha cafe DeLonghi ECAM 23.460:

Tham khảo: https://hiyams.com/may-pha-che-cafe/may-pha-cafe/may-pha-cafe-delonghi/may-pha-cafe-delonghi-ecam-23-460/

Máy pha cafe DeLonghi ECAM 23.460 thuộc dòng máy pha cafe tự động. Tích hợp một máy xay với 13 chế độ xay. Để đảm bảo rằng bột cafe được xay mịn và đều nhất có thể và chiết xuất hương thơm tốt nhất.
Máy pha cafe DeLonghi ECAM 23.460 có 2 hệ thống làm nóng, được thiết kế để kiểm soát hoàn toàn nhiệt độ của thức uống đã pha chế.
Máy pha cafe DeLonghi ECAM 23.460 còn có hệ thống tạo bọt sữa Latte Crema cải tiến với công nghệ hai hệ thống thủy lực. Nhờ đó, bạn có thể pha chế cafe theo sở thích của mình chỉ bằng một nút bấm.
Thông số kỹ thuật Máy pha cafe DeLonghi ECAM 23.460:
Kích thước (r x s x c cm): 43.0x23.8x34.0
Trọng lượng (Kg): 9
Điện áp định mức / Tần số (V ~ Hz): 220/240~50/60
Công suất đầu vào (W): 1450
Kiểm soát hương vị: ✓
Tín hiệu âm thanh: ✓
Vệ sinh tự động: ✓
Tự động tắt: ✓
Chỉ báo vệ sinh hệ thống pha cà phê cappuccino: ✓
Sử dụng bột cà phê: ✓
Bộ phận giữ tách: ✓
Chiếu sáng tách: ✓
Hệ thống De'Longhi LatteCrema: ✓
Chương trình tẩy cặn, cọ rửa và súc sạch: ✓
Hiển thị: 2 dòng với các biểu tượng
Công tắc tiết kiệm điện năng: ✓
Máy xay mới với 13 nấc điều chỉnh: ✓
Công tắc nguồn điện: ✓
Chức năng bật có thể lập trình: ✓
Độ cứng của nước có thể lập trình: ✓
Khay hứng nước nhỏ giọt có thể tháo rời có in chỉ số mực nước: ✓
Bộ phận pha có thể tháo rời: ✓
Bình đựng nước có thể tháo rời: ✓
Chọn 1 tách: ✓
Chọn 2 tách: ✓
Chức năng tạm nghỉ: ✓
Bộ lọc nước: ✓
Áp lực bơm (bar): 15
Dung tích bình chứa hạt cà phê: 250g
Mức rót tối đa/tối thiểu: 86-142 mm
Ngăn đựng cà phê bột (n°): 14
Bảo hành: 1 năm